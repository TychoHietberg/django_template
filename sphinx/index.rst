.. "{{ project_name }}" documentation master file, created by
   sphinx-quickstart on Fri Sep 25 09:51:06 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to "{{ project_name }}"'s documentation!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   auto/modules
   auto/users



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
