"""Models for users"""
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    """User model"""

    def __str__(self):
        """descriptor"""
        return f"{self.username}"
