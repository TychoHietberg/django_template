#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys
import environ


def main():
    """Run administrative tasks."""
    root_dir = environ.Path(__file__) - 1
    env = environ.Env()
    env.read_env(str(root_dir.path(".env")))

    try:
        # pylint: disable=wrong-import-order,import-outside-toplevel
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc

    if "startapp" in sys.argv:
        # set the `apps` directory as the directory for new apps
        os.chdir("apps")
        print("Do not forget to add your new app to sphinx/index.rst")
    execute_from_command_line(sys.argv)


if __name__ == "__main__":
    main()
