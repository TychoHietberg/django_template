#!/bin/bash

VIRTUAL=false

# validate that postgresql is installed
if ! command -v psql &> /dev/null
then
    printf "psql could not be found please install postgresql before running this script"
    exit
fi

printf "Would you like to create the virtual environment '{{ project_name }}'?
Please note this will install/upgrade virtualenvwrapper!
If you would prefer to use a different library or name and you are not yet working on it:
-quit this script (Ctrl-c) and start this script in the intended virtual environment.\n\n"
read -p "Create virtual environment for '{{ project_name }}'? (Y/n):" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    pip3 install virtualenvwrapper -U
    source $(which virtualenvwrapper.sh)
    mkvirtualenv -p $(which python3.9) '{{ project_name }}'
    workon '{{ project_name }}'
    setvirtualenvproject
    VIRTUAL=true
fi


DJANGO_KEY='{{ secret_key }}'

# Get database name or use project name
read -p "Specify the database name or leave blank to use default ({{ project_name|lower }})>: " DB_NAME
DB_NAME=${DB_NAME:-'{{ project_name|lower }}'}


export PGUSER='dev'
export PGPASSWORD='devdevdev'

# validate if database does not exist
while  psql -lqt | cut -d \| -f 1 | grep -qw "$DB_NAME"
do
	read -p "The database '$DB_NAME' already exists; please provide a new database name: " DB_NAME
	DB_NAME=$DB_NAME
done

# clean django key to escape special character
SAFE_DJANGO_KEY=$(printf '%s\n' $DJANGO_KEY | sed -e 's/[\/&]/\\&/g')

read -p "Specify localport (or use default 8000)>: " PORT
PORT=${PORT:-'{{ 8000 }}'}

# Generate .env file
sed "s/DJANGO_SECRET_KEY=/DJANGO_SECRET_KEY=$SAFE_DJANGO_KEY/g; s/DATABASE_NAME=/DATABASE_NAME=$DB_NAME/g; s/PORT=/PORT=$PORT/g" .env.example > .env

# install base requirements
pip install -r requirements/base.txt

# update README.md
{
  printf "### {{ project_name }}\n\n"
  printf "<!-- \nPlease enter information regarding this project.\n\n"
  printf "To learn how to use markdown:\n\n"
  printf " https://docs.gitlab.com/ee/user/markdown.html \n-->\n\n"
  printf " ##### Local Installation\n\n"
  printf " ###### Virtual Environment\n"
  printf "     workon {{ project_name }}\n"
  printf " ###### Git clone\n"
  printf "     git clone git@github.com:sociossports/{{ project_name }}.git\n"
  printf " ###### Environmental Variables\n"
  printf "     $ cp .env.example .env\n"
  printf "     $ nano .env"
  printf "\n"
  printf " ###### update Sphinx documentation\n"
  printf "     $ cmd/local/docs.sh\n"
  printf "\n"

} > README.md

# create database
createdb "$DB_NAME"

# Migrate initial tables
python3 manage.py migrate

# create superuser
python3 manage.py createsuperuser --username admin --email admin@sociossports.nl --password admin --noinput

# create sphinx documentation for apps
for dir in apps/*/; do
  sphinx-apidoc -o sphinx/auto "$dir" ./*migrations
done
sphinx-build -b html sphinx docs

if $VIRTUAL eq true
then
  printf "Before running any commands please activate the virtual environment: \n\$ workon {{ project_name}}\n"
  sleep 2
fi
echo

printf "Installation complete! you should now be able to use runserver!\n"
# remove this script
rm -- "$0"