#!/bin/bash

# to use this script in the ci runner please add ci as an argument

if [[ $1 == "ci" ]]
  then
  time pip list --outdated --format=columns --not-required| grep -v "Django"
  test=$(pip list --outdated  --not-required| grep -v "Django")


  if [[ $test ]] ; then
        echo "Please update the outdated packages"
        exit 1
  fi
  else
    time pip list --outdated --format=columns | grep -v "Django"
fi
