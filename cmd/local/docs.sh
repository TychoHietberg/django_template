#!/bin/bash
sphinx-apidoc -o sphinx/auto ./apps/  */migrations -f
sphinx-build -b html sphinx docs
read -p "Commit to git now (Y/n)?" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    git reset
    git add docs/
    git add sphinx/
    git commit -m ":books: Update documentation"
fi

