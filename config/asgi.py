"""
ASGI config for {{ project_name }} project.

It exposes the ASGI callable as a module-level variable named ``application``.

This will be available from Django 3.1 upwards

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/asgi/
"""

# from django.core.asgi import get_asgi_application
#
# import environ
# from pathlib import Path
#
# ROOT_DIR = Path(__file__).parent.parent
#
# env = environ.Env()
# env.read_env(str(ROOT_DIR.joinpath(".env")))
#
# application = get_asgi_application()
