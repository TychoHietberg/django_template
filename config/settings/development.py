"""
Settings for development environment
"""
# pylint: disable=wildcard-import, unused-wildcard-import, import-error
from .base import *  # noqa: F401,F403
from django.core.management.commands.runserver import Command as runserver

ALLOWED_HOSTS = ["*"]
try:
    runserver.default_port = DOMAIN = env("PORT")        # <-- Your port
except Exception as e:
    print(f"Development port could not be set: {e}")
    pass

runserver.default_addr = '0'   # <-- Your address

SECURE_SSL_REDIRECT = False
SESSION_COOKIE_SECURE = False
