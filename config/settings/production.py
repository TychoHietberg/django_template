"""
Settings for production environment
"""
# pylint: disable=wildcard-import, unused-wildcard-import, import-error
from .base import *  # noqa: F403

# pylint: disable=self-assigning-variable, undefined-variable
DOMAIN = DOMAIN  # noqa: F405

# pylint: disable=self-assigning-variable, undefined-variable
EMAIL_SUBJECT_PREFIX = EMAIL_SUBJECT_PREFIX  # noqa: F405

ALLOWED_HOSTS = [f"{DOMAIN}.nl", f"www.{DOMAIN}.nl"]

EMAIL_SUBJECT_PREFIX += "[PRODUCTION] "
