"""
Base settings. These are settings specific to the project.
If necessary, you can overwrite settings from defaults.py here.
"""

# pylint: disable=wildcard-import, unused-wildcard-import, import-error
from .defaults import *  # noqa: F403

# pylint: disable=self-assigning-variable, undefined-variable
INSTALLED_APPS = INSTALLED_APPS  # noqa: F405

# OVERWRITTEN from defaults.py
# -----------------------------------------------------------------------------

# LOCAL APPS
# -----------------------------------------------------------------------------
INSTALLED_APPS += ["users"]

# CRISPY FORMS
# -----------------------------------------------------------------------------
# https://django-crispy-forms.readthedocs.io/en/latest/

INSTALLED_APPS += ["crispy_forms"]
CRISPY_TEMPLATE_PACK = "bootstrap4"

# Django Registration (redux)
# -----------------------------------------------------------------------------
# https://django-registration-redux.readthedocs.io/en/latest/

INSTALLED_APPS += ["registration"]
ACCOUNT_ACTIVATION_DAYS = 7
REGISTRATION_AUTO_LOGIN = True
INCLUDE_AUTH_URLS = False
