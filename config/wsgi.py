"""
WSGI config for {{ project_name }} project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

from pathlib import Path

from django.core.wsgi import get_wsgi_application

import environ


ROOT_DIR = Path(__file__).parent.parent

env = environ.Env()
env.read_env(str(ROOT_DIR.joinpath(".env")))

application = get_wsgi_application()
