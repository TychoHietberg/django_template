### Django template project

##### Please use this project to create a new Socios Django project.


To use this template you can use the following command:


    django-admin startproject <new_project_name> 
    --template=https://repo.go2people.nl/tycho/djangoprojecttemplate/-/archive/main/djangoprojecttemplate-main.tar 
    --extension py,flake8,txt,example,sh,rst


Please note that you will need django installed to create the project.


The requirements file will have the correct version for the project listed and 
should replace the one you used in your virtualenv.


To allow easy setup you can use the initial.sh script in the project root folder.

###### Database

Please note that you will need Postgresql installed on the system and is 
accessible as a trust connection without user/password details.

If you have a setup that makes using a trust difficult you could also add the 
credentials to you Environment Variables:


    export PGUSER="username"
    export PGPASSWORD="password"
    export PGHOST="127.0.0.1"
    export PGPORT="5432"

Obviously you will need to replace the values with the values 
applicable to your database.
